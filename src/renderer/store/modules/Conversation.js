const state = {
    msgA: "",
    msgB: "",
  }
  
  const mutations = {
    setMsgA (state, msgA) {
      state.msgA=msgA
    },
    setMsgB (state, msgB) {
        state.msgB = msgB
      }
  }
  
  const getters= {
    msgA: state => state.msgA,
    msgB: state => state.msgB,
  }
  
  const actions = {
    someAsyncTask ({ commit }) {
      // do something async
      commit('setMsgA')
    }
  }
  
  export default {
    state,
    mutations,
    getters,
    actions
  }
  